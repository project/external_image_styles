# External Image Styles

## Important notes

This module does not provide any direct integration with external
image style providers, by itself! You must pair this API module with
a contrib module or, more likely, write your own `ImageStyleProvider`
plugin given your specific business rules. This module is not for
novice developers.

## Plugin contract

See the `ImageStyleProviderInterface` and `Annotation\ImageStyleProvider`
documentation for the plugin service contract.

## Cache metadata

While not supported by core, image style providers may generate cacheable
URLs, which may be consumed by cache-aware logic such as that provided
by Consumer Image Styles.
