<?php

declare(strict_types=1);

namespace Drupal\external_image_styles\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines the image style provider annotation object.
 *
 * Plugin namespace: Plugin\ImageStyleProvider.
 *
 * @see plugin_api
 *
 * @Annotation
 */
final class ImageStyleProvider extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The plugin label.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

}
