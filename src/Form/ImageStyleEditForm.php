<?php

declare(strict_types=1);

namespace Drupal\external_image_styles\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\image\Form\ImageStyleEditForm as CoreImageStyleEditForm;

class ImageStyleEditForm extends CoreImageStyleEditForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    if (is_null($this->entity->getImageStyleProvider())) {
      return parent::form($form, $form_state);
    }
    // Delegate previews to the image style provider plugin, since core assumes
    // the image style can read from a path relative to the Drupal application
    // root. The parent form calls the renderer so we can't build there, and
    // then alter here; we must build the whole form here.
    // @see https://www.drupal.org/project/drupal/issues/3218514
    $form['#title'] = $this->t('Edit style %name', ['%name' => $this->entity->label()]);
    $form['#tree'] = TRUE;

    $this->entity->getImageStyleProvider()->processEditForm($form, $form_state);

    // Add #process and #after_build callbacks.
    // @see \Drupal\Core\Entity\EntityForm::form()
    $form['#process'][] = '::processForm';
    $form['#after_build'][] = '::afterBuild';
    return $form;
  }

}
