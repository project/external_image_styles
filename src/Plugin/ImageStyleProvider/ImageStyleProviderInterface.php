<?php

declare(strict_types=1);

namespace Drupal\external_image_styles\Plugin\ImageStyleProvider;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Form\FormStateInterface;

interface ImageStyleProviderInterface extends PluginInspectionInterface {

  /**
   * Gets the image style provider label.
   *
   * @return string
   *   The provider label.
   */
  public function getLabel();

  /**
   * Returns the URL of this image derivative for an original image path or URI.
   *
   * @param string $path
   *   The path or URI to the original image.
   * @param mixed $clean_urls
   *   (optional) Whether clean URLs are in use.
   *
   * @return string
   *   The absolute URL where a style image can be downloaded, suitable for use
   *   in an <img> tag. Requesting the URL will cause the image to be created.
   *
   * @see \Drupal\image\Controller\ImageStyleDownloadController::deliver()
   * @see \Drupal\Core\File\FileUrlGeneratorInterface::transformRelative()
   */
  public function buildUrl($path, $clean_urls = NULL);

  /**
   * Flushes cached media for this style.
   *
   * @param string $path
   *   (optional) The original image path or URI. If it's supplied, only this
   *   image derivative will be flushed.
   *
   * @return $this
   */
  public function flush($path = NULL);

  /**
   * Determines the dimensions of this image style.
   *
   * Stores the dimensions of this image style into $dimensions associative
   * array. Implementations have to provide at least values to next keys:
   * - width: Integer with the derivative image width.
   * - height: Integer with the derivative image height.
   *
   * @param array $dimensions
   *   Associative array passed by reference. Implementations have to store the
   *   resulting width and height, in pixels.
   * @param string $uri
   *   Original image file URI. It is passed in to allow effects to
   *   optionally use this information to retrieve additional image metadata
   *   to determine dimensions of the styled image.
   *   ImageStyleInterface::transformDimensions key objective is to calculate
   *   styled image dimensions without performing actual image operations, so
   *   be aware that performing IO on the URI may lead to decrease in
   *   performance.
   *
   * @see ImageEffectInterface::transformDimensions
   */
  public function transformDimensions(array &$dimensions, $uri);

  /**
   * Determines the extension of the derivative without generating it.
   *
   * @param string $extension
   *   The file extension of the original image.
   *
   * @return string
   *   The extension the derivative image will have, given the extension of the
   *   original.
   */
  public function getDerivativeExtension($extension);

  /**
   * Determines if this style can be applied to a given image.
   *
   * @param string $uri
   *   The URI of the image.
   *
   * @return bool
   *   TRUE if the image is supported, FALSE otherwise.
   */
  public function supportsUri($uri);

  /**
   * Process the image style edit form.
   *
   * @param array $form
   *   Form array.
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   Form state.
   */
  public function processEditForm(array &$form, FormStateInterface $formState);

}
