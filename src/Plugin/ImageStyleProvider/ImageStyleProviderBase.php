<?php

declare(strict_types=1);

namespace Drupal\external_image_styles\Plugin\ImageStyleProvider;

use Drupal\Core\Plugin\PluginBase;

abstract class ImageStyleProviderBase extends PluginBase implements ImageStyleProviderInterface {

  /**
   * {@inheritdoc}
   */
  public function getLabel() {
    return $this->pluginDefinition['label'];
  }

  /**
   * {@inheritDoc}
   */
  public function supportsUri($uri) {
    // Assume the remote service is capable.
    return TRUE;
  }

}
