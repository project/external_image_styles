<?php

declare(strict_types=1);

namespace Drupal\external_image_styles;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\external_image_styles\Annotation\ImageStyleProvider;
use Drupal\external_image_styles\Plugin\ImageStyleProvider\ImageStyleProviderInterface;

class ImageStyleProviderManager extends DefaultPluginManager {

  /**
   * Constructs a new ImageStyleProviderManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   The cache backend.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/ImageStyleProvider', $namespaces, $module_handler, ImageStyleProviderInterface::class, ImageStyleProvider::class);

    $this->alterInfo('external_image_styles_provider_alter');
    $this->setCacheBackend($cache_backend, 'external_image_styles_provider');
  }

  /**
   * {@inheritdoc}
   */
  public function getDefinitions() {
    $definitions = parent::getDefinitions();
    if (isset($definitions['_drupal'])) {
      throw new \RuntimeException('Image style provider ID "_drupal" is reserved.');
    }
    return $definitions;
  }

}
