<?php

declare(strict_types=1);

namespace Drupal\external_image_styles;

use Drupal\Core\GeneratedUrl;

interface CacheableUrlImageStyleInterface {

  /**
   * Get a GeneratedUrl (which allows for cacheable metadata) given a Uri.
   *
   * @param string $uri
   *   Uri for image generation.
   *
   * @return \Drupal\Core\GeneratedUrl
   *   Generated Url.
   */
  public function buildGeneratedUrl(string $uri): GeneratedUrl;

}
