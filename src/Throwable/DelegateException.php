<?php

declare(strict_types=1);

namespace Drupal\external_image_styles\Throwable;

use Drupal\facets\Exception\Exception;

/**
 * "Exception" to throw when it is acceptable to delegate a method call to
 * ImageStyleProviderInterface back to the corresponding ImageStyleInterface
 * methods.
 */
class DelegateException extends Exception {}
