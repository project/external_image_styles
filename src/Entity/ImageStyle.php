<?php

declare(strict_types=1);

namespace Drupal\external_image_styles\Entity;

use Drupal\Core\Cache\Cache;
use Drupal\Core\GeneratedUrl;
use Drupal\external_image_styles\CacheableUrlImageStyleInterface;
use Drupal\external_image_styles\Plugin\ImageStyleProvider\ImageStyleProviderInterface;
use Drupal\external_image_styles\Throwable\DelegateException;
use Drupal\image\Entity\ImageStyle as CoreImageStyle;

/**
 * Overridden implementation of the ImageStyle config entity.
 *
 * Delegated execution of methods allows for polymorphism on on the image
 * style provider, e.g. for passing additional information from a (further
 * overridden) image styles provider service to the external provider's methods.
 */
class ImageStyle extends CoreImageStyle implements CacheableUrlImageStyleInterface {

  /**
   * Image style provider.
   *
   * @var \Drupal\external_image_styles\Plugin\ImageStyleProvider\ImageStyleProviderInterface|NULL
   */
  protected ?ImageStyleProviderInterface $imageStyleProvider = NULL;

  /**
   * Getter for the image style provider.
   *
   * @return \Drupal\external_image_styles\Plugin\ImageStyleProvider\ImageStyleProviderInterface|NULL
   */
  public function getImageStyleProvider(): ?ImageStyleProviderInterface {
    return $this->imageStyleProvider;
  }

  /**
   * Delegate method calls to the image style provider.
   *
   * @param string $method
   *   Method name.
   * @param array $arguments
   *   Array of original arguments.
   *
   * @return mixed
   *   Method call result.
   */
  protected function delegateCall(string $method, array $arguments) {
    if (!is_null($this->imageStyleProvider)) {
      try {
        return $this->imageStyleProvider->{$method}(...$arguments);
      }
      catch (DelegateException $e) {
        return parent::$method(...$arguments);
      }
    }
    return parent::$method(...$arguments);
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(array $values, $entity_type) {
    parent::__construct($values, $entity_type);
    if ($pluginId = $this->getThirdPartySetting('external_image_styles', 'provider')) {
      /** @var \Drupal\external_image_styles\ImageStyleProviderManager $providerManager */
      $providerManager = \Drupal::service('external_image_styles.provider_manager');
      $this->imageStyleProvider = $providerManager->createInstance(
        $pluginId,
        ['image_style' => $this]
      );
    }
  }

  /**
   * {@inheritdoc}
   */
  public function buildUri($uri) {
    if (!is_null($this->imageStyleProvider)) {
      throw new \RuntimeException(sprintf('Call to %s is unsupported; derivative creation is external. Check your site configuration. Use ::buildUrl or ::buildGeneratedUrl', __METHOD__));
    }
    return parent::buildUri($uri);
  }

  /**
   * {@inheritdoc}
   */
  public function buildUrl($path, $clean_urls = NULL) {
    return $this->delegateCall(__FUNCTION__, func_get_args());
  }

  /**
   * Build a GeneratedUrl; if unsupported, create one that is permanently
   * cacheable (which is basically what buildUrl() gives.
   *
   * @param string $uri
   *   URI for image derivative generation.
   *
   * @return \Drupal\Core\GeneratedUrl
   *   Generated URL object.
   */
  public function buildGeneratedUrl(string $uri): GeneratedUrl {
    if ($this->imageStyleProvider instanceof CacheableUrlImageStyleInterface) {
      return $this->delegateCall(__FUNCTION__, func_get_args())
        ->addCacheableDependency($this);
    }
    return (new GeneratedUrl())
      ->setCacheMaxAge(Cache::PERMANENT)
      ->addCacheableDependency($this)
      ->setGeneratedUrl($this->buildUrl(...func_get_args()));
  }

  /**
   * {@inheritdoc}
   */
  public function flush($path = NULL) {
    return $this->delegateCall(__FUNCTION__, func_get_args());
  }

  /**
   * {@inheritdoc}
   */
  public function createDerivative($original_uri, $derivative_uri) {
    if (!is_null($this->imageStyleProvider)) {
      throw new \RuntimeException(sprintf('Call to %s is unsupported; derivative creation is external. Check your site configuration.', __METHOD__));
    }
    return parent::createDerivative($original_uri, $derivative_uri);
  }

  /**
   * {@inheritdoc}
   */
  public function transformDimensions(array &$dimensions, $uri) {
    $args = func_get_args();
    array_shift($args);
    return $this->delegateCall(__FUNCTION__, [&$dimensions, ...$args]);
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeExtension($extension) {
    return $this->delegateCall(__FUNCTION__, func_get_args());
  }

  /**
   * {@inheritdoc}
   */
  public function supportsUri($uri) {
    return $this->delegateCall(__FUNCTION__, func_get_args());
  }

}
